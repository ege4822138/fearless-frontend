import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";

import AttendeesList  from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './conferenceForm';
import AttendConferenceForm from './AttendeesForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

import Nav from './Nav'

function App(props) {
  if(props.attendees === undefined){
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList attendees={props.attendees} />} />
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
