import React, { useEffect, useState } from "react";

function ConferenceForm(props){

    const [name, setName] = useState('');
    const [starts, setStartDate] = useState('');
    const [ends, setEndDate] = useState('');
    const [description, setDescription] = useState('');
    const [presentations, setPresentations] = useState('');
    const [attendees, setAttendeesDate] = useState('');
    const [location, setLocation] = useState('');
    const [conferences, setConferences] = useState([]);



    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
    };

    const handleStartChange = (event) => {
      const value = event.target.value;
      setStartDate(value);
    };

    const handleEndChange = (event) => {
      const value = event.target.value;
      setEndDate(value);
    };

    const handleDescriptionChange = (event) => {
      const value = event.target.value;
      setDescription(value);
    };

    const handlePresentationsChange = (event) => {
      const value = event.target.value;
      setPresentations(value);
    };

    const handleAttendeesChange = (event) => {
      const value = event.target.value;
      setAttendeesDate(value);
    };

    const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
    };

    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};
      data.name = name;
      data.starts = starts;
      data.ends = ends;
      data.description = description;
      data.max_presentations = presentations;
      data.max_attendees = attendees;
      data.location = location;
      console.log(data);


    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method:"post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if(response.ok){
      const newConference = await response.json();
      console.log(newConference);

      setName('');
      setStartDate('');
      setEndDate('');
      setDescription('');
      setPresentations('');
      setAttendeesDate('');
      setLocation('');
    }
  }



    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";

        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setConferences(data.locations)
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
          <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} value={starts} placeholder="mm/dd/yyyy" required type="date" name="starts" id="starts" className="form-control" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} name="description" id="description" className="form-control" rows="4"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePresentationsChange} value={presentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleAttendeesChange} value={attendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {conferences.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
